// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below

const vscode = require('vscode');
const chooseTemp = require('./src/template')
const webview = require('./src/webview')

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "bootstrap-layout-template" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('bootstrap-layout-template.blog', async function () {
		// The code you place here will be executed every time your command is executed
		vscode.window.createTerminal("Select Template")
		let input = await vscode.window.showInputBox({
			placeHolder: "Input Template",
			prompt: "Input 'help' for more information"
		})
		input = input.toLowerCase()
		if (input == "help") {
			const panel = vscode.window.createWebviewPanel(
				'help',
				'Template List',
				vscode.ViewColumn.One,
				{
					localResourceRoots: [vscode.Uri.file('./src')]
				}
			  );
			panel.webview.html = webview
		}
		else{
			let text = chooseTemp(input)
			if(text != null){
				await vscode.workspace.openTextDocument({
					language: "html",
					content: text
				})
				vscode.window.showInformationMessage(`Success import ${input} template!!!`);
			}
			else{
				vscode.window.showInformationMessage(`Failed to import ${input} template, Template not Found!!!`);
			}
		}
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
