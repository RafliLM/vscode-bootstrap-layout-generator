const webview = 
`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cat Coding</title>
</head>
<body>
    <h1>Template List</h1>
    <ul>
        <li>Analytics</li>
        <img src="https://i.ibb.co/wphhB7R/analytics.jpg" alt="analytics" width="25%" height="auto">
        <li>Blog</li>
        <img src="https://i.ibb.co/xMDLGQ0/blog.jpg" alt="blog" width="25%" height="auto">
        <li>Marketing</li>
        <img src="https://i.ibb.co/xsHdGCz/marketing.jpg" alt="marketing" width="25%" height="auto">
        <li>Portofolio</li>
        <img src="https://i.ibb.co/1723X8j/portofolio.jpg" alt="portofolio" width="25%" height="auto">
        <li>Social</li>
        <img src="https://i.ibb.co/P9D8G9d/social.jpg" alt="social" width="25%" height="auto">
        <li>Webpage</li>
        <img src="https://i.ibb.co/Lg2mgbK/webpage.jpg" alt="webpage" width="25%" height="auto">
    </ul>
</body>
</html>
`
module.exports = webview