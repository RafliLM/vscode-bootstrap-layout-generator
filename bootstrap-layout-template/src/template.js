// template from https://www.w3schools.com/bootstrap/bootstrap_templates.asp
const blog = require('./blog')
const portofolio = require('./portofolio')
const webpage = require('./webpage')
const social = require('./social')
const marketing = require('./marketing')
const analytics = require('./analytics')

const chooseTemp = (type) => {
    switch (type) {
        case "blog":
            return blog
        case "portofolio":
            return portofolio
        case "webpage":
            return webpage
        case "social":
            return social
        case "marketing":
            return marketing
        case "analytics":
            return analytics
        default:
            return null
    }
}

module.exports = chooseTemp